/**
 * @author Ashish Shende
 *
 * This is the basic configuration of the Node.js express server
 */

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const multer  = require('multer');
const controllerClinateApi = require('./controller/ContactController')
const path = require("path");
const contactRoute = require('./route/ContactApi');

// Mongoose
// require('./db/mongoose');
require('./index');
// require('./common/insert-data-from-excel');
// app.use(bodyParser.urlencoded({extended: true}));
// app.use(bodyParser.json());
app.use(bodyParser.json({limit: "100mb"}));
app.use(bodyParser.urlencoded({limit: "100mb", extended: true, parameterLimit:50000}));

// CORS headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token');
    // Handle preflight OPTIONS request
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});



app.use('/api/v1/konverge', contactRoute);




module.exports = app