const express = require('express');
const router = express.Router();
const controllerClinateApi = require('../controller/ContactController')

router.post('/contact-us', controllerClinateApi.sendmailClient)

router.post('/upload-job-details', controllerClinateApi.uploadJobDetails)

/* This route is used to handle joi error */
router.use(function (err, req, res, next) {
    if (err.isBoom) {
        res.status(400).json({ message: err.data[0].message.replace(/['"]+/g, '') });
    }
});

module.exports = router;