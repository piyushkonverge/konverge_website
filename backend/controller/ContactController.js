const sgMail = require('@sendgrid/mail')
const config = require('config');
const multer = require("multer");
const fs = require("fs");
const path = require('path');
sgMail.setApiKey(config.get('SendGrid.SENDGRID_API_KEY'))

/* To send the mail for Clinate*/
exports.sendmailClient = (req, res, next) => {
  if (req.body.firstName === undefined || req.body.email === undefined || req.body.mobileNumber === undefined || req.body.message === undefined ||req.body.company === undefined) {
    res.status(402).json({
      message: "Something went wrong"
    })

  } else {
    const msg = {
      to:'prateek@konverge.ai',
      // to: req.body.email, // Change to your recipient
      from: req.body.email, // Change to your verified sender
      // from: 'prateek@konverge.ai', // Change to your verified sender
      subject: 'Buisness Enquiry',
      bcc:'info@konverge.ai',
      text: "First Name: " + req.body.firstName +" " + req.body.lastName +', Email: ' + req.body.email +', Company: ' + req.body.company + ', Mobile Number: ' + req.body.mobileNumber + ', Query' + req.body.message,
      html: '<strong>First Name: ' + req.body.firstName +'&nbsp;'+ req.body.lastName +'</strong><br><strong> Company Name: ' + req.body.company +'</strong><br><strong> Email: ' + req.body.email + '</strong><br>' + '<strong> Mobile Number: ' + req.body.mobileNumber + '</strong><br>' + '<strong> Message: ' + req.body.message + '</strong><br>',
    }
    sgMail
      .send(msg)
      .then(() => {
        res.status(200).json({
          message: 'Successfully sent the query'
        })
      })
      .catch((error) => {
        res.status(402).json({
          message: error.message
        })
      })

  }
}

/* defined storage and filename */
const fileStorage = multer.diskStorage({

  destination: (req, file, cb) => {
    cb(null, "./public/uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, `${+new Date()}.` + file.originalname);
  }
});

/* defined filter */
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "application/pdf"
  ) {
    cb(null, true);
  } else {
    cb("File format should be PDF", false); // if validation failed then generate error
  }
};


const upload = multer({ storage: fileStorage, limits: { fileSize: 1000000 }, fileFilter: fileFilter });



/* To send the mail for job form with attachment*/
exports.uploadJobDetails=(req,res,next)=>{
  const fileUpload = upload.single("cv");
  fileUpload(req, res, function (err) {
    if (err) {
      console.log("err",err)
      // An error occurred when uploading 
      res.status(400).json({
        errors: {
          message: err
        },
      });
      return;
    }
    
    if (req.body.firstName === undefined || req.body.email === undefined || req.body.mobileNumber === undefined ||req.body.college === undefined) {
      res.status(402).json({
        message: "Something went wrong"
      })
  
    } else {
      console.log(path.resolve(req.file.path))
      let pathToAttachment = path.resolve(req.file.path);
      // let pathToAttachment = `${__dirname}`+'/'+req.file.path;
      let attachment = fs.readFileSync(pathToAttachment).toString("base64");
      const msg = {
        to:'prateek@konverge.ai',
        // to:'info@konverge.ai',
        from: req.body.email, // Change to your verified sender
        subject: 'Buisness Enquiry',
        bcc:'info@konverge.ai',
        text: "First Name: " + req.body.firstName +" " + req.body.lastName +', Email: ' + req.body.email +', Company: ' + req.body.college + ', Mobile Number: ' + req.body.mobileNumber,
        html: '<strong>First Name: ' + req.body.firstName +'&nbsp;'+ req.body.lastName +'</strong><br><strong> College Name: ' + req.body.college +'</strong><br><strong> Email: ' + req.body.email + '</strong><br>' + '<strong> Mobile Number: ' + req.body.mobileNumber +'</strong><br>',
        attachments: [
          {
            content: attachment,
            ContentType: 'application/pdf',
            filename: req.file.filename,
            disposition: 'attachment',
            contentId: 'mytext'
          },
        ]
      }
      sgMail
        .send(msg)
        .then(() => {
          res.status(200).json({
            message: 'Successfully sent the query'
          })
        })
        .catch((error) => {
          res.status(402).json({
            message: error.message
          })
        })
  
    }

  })

}
