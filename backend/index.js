/**
 * @author Rushikesh Gomekar
 *
 * This is the entry point of the Node.js server
 */

const app = require('./app');
// PORT
const port = process.env.PORT || 4000;

// Start the server
app.listen(port, () => {
    console.log('Server running on port*', port);
});